"""Module that handles swift conections."""

from __future__ import annotations
import base64
from datetime import datetime
from functools import partial
from getpass import getpass
import json
import logging
import multiprocessing as mp
import os
from pathlib import Path
from urllib.parse import urljoin, urlparse
import time
from typing import cast, Any, Optional, Union, Iterator

import appdirs
import requests
from swiftclient.multithreading import OutputManager
from swiftclient.service import (
    SwiftError,
    SwiftService,
    SwiftUploadObject,
)


SwiftAuthType = Optional[Union[str, bool, float, int, list[str]]]
"""Type for arguments to connect to swift storage url."""
logging.getLogger("requests").setLevel(logging.WARNING)
logging.getLogger("swiftclient").setLevel(logging.WARNING)
logger = logging.getLogger("swift-upload")


def _get_upload_object(
    inp_dir: Union[str, Path],
    path: Union[str, Path],
    options: Optional[dict[str, bool]] = None,
) -> SwiftUploadObject:
    """Helper function to create an upload object from a path."""
    if options:
        sw_upload = partial(SwiftUploadObject, options=options)
    else:
        sw_upload = SwiftUploadObject
    swift_file = Path(path).relative_to(Path(inp_dir).parent)
    return sw_upload(str(path), str(swift_file))


def _print_status(res: dict[str, str], container: str) -> None:
    """Print status of swift a operation."""
    if res["success"]:
        if "object" in res:
            logger.info(res["object"])
        elif "for_object" in res:
            logger.info(
                "%s segment %s", res["for_object"], res["segment_index"]
            )
    else:
        error = res["error"]
        if res["action"] == "create_container":
            logger.warning(
                "Warning: failed to create container " "'%s'%s",
                container,
                error,
            )
        elif res["action"] == "upload_object":
            logger.error(
                "Failed to upload object %s to container %s: %s",
                container,
                res["object"],
                error,
            )
        else:
            logger.error(error)


class Swift:
    """Estabilish a connection to a dkrz swift cloud store.

    Parameters
    ----------
    account: str
        Account name that is used as swift login
    username: str, optional
        Username to logon if not given (default) set to account name. Which
        is equivalent to logging on to the personal siwft.
    password: str, optional
        Password used to logon to the swift store. If not given (default)
        and a password is needed a password prompt will ask for the password.

        .. note:: A password will only be needed if no login authorisation
                  token exists or the token has expired.
    swift_server: str, default: swift.dkrz.de
        The url of the server hosting the cloud storage
    auth_version: int, default: 1.0
        The auhtentication version used to establish a connection to the
        swift server.


    Attributes
    ----------
    auth_token: str
        Swift authentication token.

    Example
    -------

    ::

        from swift_upload import Swift
        connection = Swift("ch1187", "k204230")
    """

    browser_url = "https://swiftbrowser.dkrz.de"

    def __init__(
        self,
        account: str,
        username: Optional[str] = None,
        password: Optional[str] = None,
        swift_server: str = "https://swift.dkrz.de",
        auth_version: float = 1.0,
    ):
        """Determine if the swift auth-token exists and is still valid."""
        _swift_status = self._get_swift_token_status(account)
        self._parsed_swift_url = urlparse(swift_server)
        self._auth_version = auth_version
        self.account = account
        self.username = username or self.account
        now = datetime.now()
        _expires: float = cast(float, _swift_status["expires"])
        try:
            expires_in = (
                datetime.fromtimestamp(_expires) - now
            ).total_seconds()
        except TypeError:
            expires_in = -1

        if _swift_status["auth_token"] is None or expires_in < 1:
            self._get_auth(self.username, password)
        elif password:
            self._get_auth(self.username, password)
        else:
            self.auth_token = cast(str, _swift_status["auth_token"])
            self.storage_url = cast(str, _swift_status["storage_url"])
            self.expires = _swift_status["expires"]
        self._write_token_to_disk()
        self._connection = SwiftService(options=self._connect_options)

    def __enter__(self) -> Swift:

        return self

    def __exit__(self, *args: Any) -> None:

        self._connection.thread_manager.__exit__(*args)

    @staticmethod
    def get_passwd(
        msg: str = "User password for cloud storage", wait_sec: int = 60
    ) -> str:
        """
        Get a password from the getpass prompt

        Parameters
        ----------
        msg:
            user defined message to be displayed when asking for the password.
        wait_sec:
            amount of seconds to wait before terminating the password request.

        Returns
        -------
        str: the password, empty if terminated.
        """
        if not wait_sec:
            return getpass(f"{msg}: ")

        def gpass(queue: mp.Queue, msg: str) -> None:
            queue.put(getpass(msg))

        queue: mp.Queue = mp.Queue()
        proc = mp.Process(
            target=gpass,
            args=(
                queue,
                f"{msg}: ",
            ),
        )
        proc.start()
        n_itt = 0
        while proc.is_alive():
            time.sleep(0.1)
            if n_itt > wait_sec * 10:
                proc.terminate()
                return ""
            n_itt += 1
        password = queue.get()
        proc.join()
        return password

    @property
    def auth_url(self) -> str:
        """Create the swift authentication url."""
        return urljoin(
            f"https://{self._parsed_swift_url.netloc}",
            f"auth/v{self._auth_version}",
        )

    def _get_auth(self, username: str, password: Optional[str]) -> None:
        """Connect to the swift store to query the authentication token."""
        headers = {
            "X-Auth-User": f"{self.account}:{username}",
            "X-Auth-Key": f"{password or self.get_passwd()}",
        }
        resp = requests.get(self.auth_url, headers=headers)
        try:
            resp.raise_for_status()
        except requests.RequestException:
            raise requests.RequestException(
                f"Connection to {self.auth_url} failed"
            ) from requests.RequestException
        self.auth_token = resp.headers["x-auth-token"]
        expires = int(resp.headers["x-auth-token-expires"])
        self.storage_url = resp.headers["x-storage-url"]
        self.expires = time.time() + expires

    @property
    def swift_file(self) -> Path:
        """File where the information to the swift connection is stored."""
        return Path(appdirs.user_data_dir()) / "swift.conf"

    def _write_token_to_disk(self) -> None:

        try:
            content = json.loads(self._get_token_file_content())
        except json.JSONDecodeError:
            content = {}
        update = {
            self.account: dict(
                auth_token=self.auth_token,
                storage_url=self.storage_url,
                expires=self.expires,
            )
        }
        content.update(update)
        self.swift_file.parent.mkdir(exist_ok=True, parents=True)
        with self.swift_file.open("bw") as f_obj:
            f_string = json.dumps(content)
            f_obj.write(base64.b64encode(f_string.encode()))
        self.swift_file.chmod(0o600)

    def _get_token_file_content(self) -> str:
        """Read the content of the token file."""
        try:
            with self.swift_file.open("r") as f_obj:
                return base64.b64decode(f_obj.read().encode()).decode()
        except FileNotFoundError:
            return "{}"

    def _get_swift_token_status(
        self, container: str
    ) -> dict[str, Optional[Union[str, float]]]:
        """Try reading the token from the disk."""

        try:
            login_info = json.loads(self._get_token_file_content())[container]
        except (json.JSONDecodeError, KeyError):
            login_info = {}
        _swift_status: dict[str, Optional[Union[str, float]]] = {}
        for key in "auth_token", "storage_url", "expires":
            try:
                _swift_status[key] = login_info[key]
            except KeyError:
                _swift_status[key] = None
        return _swift_status

    @property
    def fsspec_auth(self) -> list[dict[str, str]]:
        """Get the authentication for swift fsspec."""
        return [{"token": self.auth_token, "url": self.storage_url}]

    @property
    def _connect_options(
        self,
    ) -> dict[str, Union[SwiftAuthType, dict[str, SwiftAuthType]]]:
        return {
            "help": False,
            "os_help": False,
            "snet": False,
            "verbose": 1,
            "debug": False,
            "info": False,
            "auth": None,
            "auth_version": "2.0",
            "user": " ",
            "key": " ",
            "retries": 5,
            "insecure": False,
            "ssl_compression": True,
            "force_auth_retry": False,
            "prompt": False,
            "os_username": " ",
            "os_user_id": None,
            "os_user_domain_id": None,
            "os_user_domain_name": None,
            "os_password": " ",
            "os_tenant_id": None,
            "os_tenant_name": None,
            "os_project_id": None,
            "os_project_name": None,
            "os_project_domain_id": None,
            "os_project_domain_name": None,
            "os_auth_url": None,
            "os_auth_type": None,
            "os_application_credential_id": None,
            "os_application_credential_secret": None,
            "os_auth_token": self.auth_token,
            "os_storage_url": self.storage_url,
            "os_region_name": None,
            "os_service_type": None,
            "os_endpoint_type": None,
            "os_cacert": None,
            "os_cert": None,
            "os_key": None,
            "changed": False,
            "skip_identical": False,
            "segment_size": None,
            "segment_container": None,
            "leave_segments": False,
            "object_threads": 20,
            "segment_threads": 20,
            "meta": [],
            "header": [],
            "use_slo": False,
            "object_name": None,
            "checksum": True,
            "os_options": {
                "user_id": None,
                "user_domain_id": None,
                "user_domain_name": None,
                "tenant_id": None,
                "tenant_name": None,
                "project_id": None,
                "project_name": None,
                "project_domain_id": None,
                "project_domain_name": None,
                "service_type": None,
                "endpoint_type": None,
                "auth_token": self.auth_token,
                "object_storage_url": self.storage_url,
                "region_name": None,
                "auth_type": None,
                "application_credential_id": None,
                "application_credential_secret": None,
            },
            "object_uu_threads": 20,
        }

    @staticmethod
    def _get_filenames(inp: Path) -> list[SwiftUploadObject]:
        """Walk through a given directory and return all filenames."""

        inp = inp.expanduser().absolute()
        if inp.is_file():
            return [SwiftUploadObject(str(inp), inp.name)]
        objs = []
        dir_markers = []
        opts = {"dir_marker": True}
        for (_dir, _ds, _fs) in os.walk(inp):
            if not _ds + _fs:
                dir_markers.append(_get_upload_object(inp, _dir, options=opts))
            else:
                objs.extend(
                    [
                        _get_upload_object(inp, os.path.join(_dir, _f))
                        for _f in _fs
                    ]
                )
        return objs + dir_markers

    def stat(self, container: str) -> dict[str, str]:
        """Get the statistics of the swift cloud object store.

        Parameters
        ----------
        container: str
            Name of the swift object container.

        Returns
        -------
        dict: Dictionary holding object store information.
        """
        return dict(self._connection.stat(container).get("items", {}))

    def delete(
        self,
        container: str,
        path: str | Path | None = None,
    ) -> dict[str, Any]:
        """Delete swift object store.

        Parameters
        ----------
        container: str
            Name of the swift object conatiner.
        path: str
            name of the swift pseudo folder that should be listed.

        Returns
        -------
        dict: Dictionary holding the information on the delet request.
        """
        objects = (str(a) for a, _ in self.list(container, path))
        return list(self._connection.delete(container, objects))[0]

    def list(
        self,
        container: str,
        path: str | Path | None = None,
        dir_only: bool = False,
    ) -> Iterator[tuple[Path, datetime]]:
        """List the content of a swift container.

        Parameters
        ----------
        container: str
            Name of the swift object container.
        path: str
            name of the swift pseudo folder that should be listed.
        dir_only: bool, default: False
            List only directory objects

        Yields
        ------
        os.pathlike:
            path on the swift container
        datetime.datetime:
            datetime object representing the last modification
            of the path
        """
        prefix = None
        if path:
            try:
                prefix = str(Path(path).relative_to(container))
            except ValueError:
                prefix = str(path)
            if prefix == ".":
                prefix = None
        try:
            res = list(self._connection.list(container, dict(prefix=prefix)))[
                0
            ]
        except IndexError:
            res = {}
        root = prefix or ""
        for obj in res.get("listing", []):
            name = Path(obj["name"])
            try:
                name.relative_to(root)
            except ValueError:
                continue

            time_stamp = datetime.fromisoformat(obj["last_modified"])
            if obj["content_type"] == "application/directory":
                yield name, time_stamp
            elif dir_only is False:
                yield name, time_stamp

    def make_public(self, container: str) -> None:
        """Make a container public.

        Parameters
        ----------
        container: str
            Name of the swift object container.
        """
        status = self.stat(container)
        read_acl = ".r:*,.rlistings"
        if status["Read ACL"] != read_acl:
            self._connection.post(
                container, options={"read_acl": ".r:*,.rlistings"}
            )

    def exists(
        self, container: str, path: Optional[str | Path] = None
    ) -> bool:
        """Check if container exists and path exists.

        Parameters
        ----------
        container: str
            Name of the swift object container
        path: str | Path, default: None
            Pseudo folder in the container

        Returns
        -------
        bool: True if conater and pseudo folder exists.
        """

        object_path = Path(container) / Path(path or "")
        try:

            if self.stat(str(object_path)):
                return True
            return False
        except SwiftError:
            return False

    def get_swift_container_url(
        self,
        container: str,
        path: Optional[str | Path] = None,
        scheme: str = "swift",
    ) -> str:
        """Get the url of the swift storage object.

        Parameters
        ----------
        container: str
            Name of the swift object container
        path: str | Path, default: None
            Pseudo folder in the container
        scheme: str, default: swift
            url scheme

        Returns
        -------
        str: url of the swift object store
        """
        container_id = self.stat(container)["Account"]
        url_path = f"{container_id}/{container}"
        if path:
            url_path = f"{url_path}/{path}"
        return f"{scheme}://{self._parsed_swift_url.netloc}/{url_path}"

    def create_container(self, container: str) -> int:
        """Create a new continer swift container."""
        try:
            resp = list(self._connection.upload(container, []))
        except SwiftError as error:
            logger.error("Could not create container: %s", error)
            return 401
        return resp[0].get("response_dict", {}).get("status", 401)

    def upload(
        self,
        inp_dir: Union[str, Path],
        container: str,
        public: bool = True,
    ) -> str:
        """
        Upload a folder to a given swift-container.

        Parameters
        ----------
        inp_dir: str, pathlib.Path
            Source input folder that is going to be uploaded
        container: str
            Name of the swift container
        public: bool, default: True
            Make the new container public

        Returns
        -------
        str: url to the newly created swift storage object.
        """
        inp_dir = Path(inp_dir).expanduser().absolute()
        parent = inp_dir.name
        if inp_dir.is_dir():
            parent += "/"

        with OutputManager():
            objs = self._get_filenames(inp_dir)
            try:
                for res in self._connection.upload(container, objs):
                    _print_status(res, container)
            except SwiftError as error:
                logger.error(error.value)
        if public:
            self.make_public(container)
        container_url = self.get_swift_container_url(container, scheme="https")
        return f"{container_url}/{parent}"
