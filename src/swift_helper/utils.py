"""Collection of utilities."""

from typing import Any

import aiohttp
import aiohttp_retry


async def get_client(**kwargs: Any) -> aiohttp_retry.RetryClient:
    retry_options = aiohttp_retry.ExponentialRetry(
        attempts=3,
        exceptions={OSError, aiohttp.ServerDisconnectedError},
    )
    retry_client = aiohttp_retry.RetryClient(
        raise_for_status=False, retry_options=retry_options
    )
    return retry_client
