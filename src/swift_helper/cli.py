"""Command line interface to run the swift uploader."""

from __future__ import annotations
import argparse
from getpass import getuser
import grp
import logging
from pathlib import Path
import pwd


from . import __version__, logger, Swift


def cli() -> None:
    """Construct the command line interface."""

    cli_app = argparse.ArgumentParser(
        prog="swift-uploader",
        description="Upload content to the dkrz cloud store",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    cli_app.add_argument(
        "paths",
        nargs="+",
        type=Path,
        help="Input paths",
    )
    cli_app.add_argument(
        "-u",
        "--user",
        help="Set the login username.",
        default=getuser(),
        type=str,
    )
    cli_app.add_argument(
        "-a",
        "--account",
        help="Set the swift login account/group.",
        default=grp.getgrgid(pwd.getpwnam(getuser()).pw_gid).gr_name,
        type=str,
    )
    cli_app.add_argument(
        "-v",
        "--verbose",
        action="count",
        help="Verbosity level",
        default=0,
    )
    cli_app.add_argument(
        "-V",
        "--version",
        action="version",
        version="%(prog)s {version}".format(version=__version__),
    )
    args = cli_app.parse_args()
    log_level = max(logging.DEBUG, logging.ERROR - 10 * args.verbose)
    logger.setLevel(log_level)
    logging.getLogger("requests").setLevel(log_level)
    logging.getLogger("swiftclient").setLevel(log_level)
    connection = Swift(args.account, args.user)
    for path in args.paths:
        connection.upload(path, path.name)
