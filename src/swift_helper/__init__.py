"""Interact with the content of the dkrz cloud storage.

The module lets you logon to the openstack dkrz cloud storage and interact
with the content on it.
"""


from .swift import Swift, logger


__version__ = "2022.11.02"
__all__ = ["Swift", "logger"]
