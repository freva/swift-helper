# makefile used for testing
#
#
all: install test

.PHONY: docs

lint:
	mypy --install-types --non-interactive
	black --check -t py310 -l 79 src
	flake8 src/swift_helper --count --max-complexity=15 --max-line-length=88 --statistics --doctests
#	stubgen -p swift_helper -o src/
