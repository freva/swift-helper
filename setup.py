#!/usr/bin/env python3
"""Setup script for packaging checkin."""

from pathlib import Path
import re
from setuptools import find_packages, setup


def read(*parts):
    """Read the content of a file."""
    script_path = Path(__file__).parent
    with script_path.joinpath(*parts).open() as f:
        return f.read()


def find_version(*parts: str) -> str:
    vers_file = read(*parts)
    match = re.search(r'^__version__ = "(\d+.\d+.\d+)"', vers_file, re.M)
    if match is not None:
        return match.group(1)
    raise RuntimeError("Unable to find version string.")


setup(
    name="swift_helper",
    version=find_version("src", "swift_helper", "__init__.py"),
    author="Martin Bergemann",
    author_email="bergemann@dkrz.de",
    maintainer="Martin Bergemann",
    url="https://gitlab.dkrz.de/k204230/checkin.git",
    description="Interact with the DKRZ swift cloud storage",
    long_description=read("README.md"),
    long_description_content_type="text/markdown",
    license="GPLv3",
    packages=find_packages("src"),
    package_data={"": ["py.typed"]},
    package_dir={"": "src"},
    install_requires=[
        "aiohttp-retry",
        "appdirs",
        "python-swiftclient",
        "swiftspec",
        "requests",
    ],
    extras_require={
        "tests": [
            "black",
            "mypy",
            "flake8",
        ],
        "docs": [
            "sphinx",
            "nbsphinx",
            "recommonmark",
            "myst-parser",
            "sphinx_rtd_theme",
            "ipython",  # For nbsphinx syntax highlighting
            "sphinxcontrib_github_alt",
            "pandoc",
        ],
    },
    python_requires=">=3.7",
    # entry_points={"console_scripts": ["swift-upload = swift_upload.cli:cli"]},
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Environment :: Console",
        "Intended Audience :: Developers",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: BSD License",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python :: 3",
    ],
)
