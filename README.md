# Swift Helper

`swift_helper` is a small python library written on top of the
`swiftclient` library to interact with the OpenStack swift cloud
storage system. With help of this library you can easily `list`, `remove`,
`upload` and `download` object on the swift cloud store.


The code can be used as a library to interact with the openstack swift object store.

## Installation
```console
pip install swift-helper --extra-index-url gitlab.dkrz.de/api/v4/projects/139460/packages/pypi/simple
```
## Usage

The usage of the library is relatively simple:

```python
from swift_upload import Swift
swift_group = "my_group"
user_name = "b12345"
upload_path = "~/path/to/content"
swift_container = "container_name"

connection = Swift(swift_group, user_name)
connection.upload(upload_path, swift_container)
```

For more information [![The Docs](https://img.shields.io/badge/Read-Docs-green.svg)](https://freva.gitlab-pages.dkrz.de/swift-helper)
