.. swift-helper documentation master file, created by
   sphinx-quickstart on Thu Nov 10 14:29:23 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to swift-helper's documentation!
========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:


:py:mod:`swift_helper` is a small python library written on top of the
:py:mod:`swiftclient` library to interact with the OpenStack swift cloud
storage system. With help of this library you can easily ``list``, ``remove``,
``upload`` and ``download`` object on the swift cloud store.

Installation
============

The library can be installed via pip

.. code:: python

    python -m pip install swift-helper --extra-index-url gitlab.dkrz.de/api/v4/projects/139460/packages/pypi/simple

Quick Guide
===========
Using the library is relatively straight forward. First you will have to create
a swfit session using your login credentials and the so called swift account.
The swift account is the data project group this account is associated with.

.. note::

    For private accounts you can set your username as account.


.. code:: python

    from swift_helper import Swfit
    sw = Swift("account", "username")
    connection.upload("~/path_to_upload", "name_of_desired_swift_container")


A detailed documentation can be found below:

API Reference
=============
.. automodule:: swift_helper
    :members:
    :show-inheritance:

.. seealso::

    Module :py:mod:`swiftclient`
        Python library to interact with the swift object store
    Module :py:mod:`swiftspec`
        Python extension for :py:mod:`fsspec`

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
